require "spec_helper"
require "birth_date_from_lv_personal_code"

describe BirthDateFromLvPersonalCode do

  let(:subject) {BirthDateFromLvPersonalCode.new}

  it 'return correct date' do
    expect(subject.to_birth_date("110388-12312")).to eq(Date.new(1988, 3, 11))
  end

  it 'return nil' do
    expect(subject.to_birth_date("")).to be_nil
  end

  describe "cenuries" do

    it '1800' do
      expect(subject.to_birth_date("110388-02312")).to eq(Date.new(1888, 3, 11))
    end

    it '1900' do
      expect(subject.to_birth_date("110388-12312")).to eq(Date.new(1988, 3, 11))
    end

    it '2000' do
      expect(subject.to_birth_date("110388-22312")).to eq(Date.new(2088, 3, 11))
    end

  end

end
