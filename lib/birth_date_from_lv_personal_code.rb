require "birth_date_from_lv_personal_code/version"

# country is Latvia
# e.g. 110388-12345
class BirthDateFromLvPersonalCode

  CENTURIES = {
    '0' => "18",
    '1' => "19",
    '2' => "20",
  }

  def self.to_birth_date(personal_code)
    begin

      year = "#{CENTURIES[personal_code[7]]}#{personal_code[4]}#{personal_code[5]}"
      birth_string = "#{personal_code[0..3]}#{year}"
      Date.strptime(birth_string, "%d%m%Y")

    rescue => e

      nil

    end
  end

end
